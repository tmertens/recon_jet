package com.example.android.bluetoothchat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.File;

/**
 * Created by Thomas on 12.07.2015.
 */
public class FileExplorerActivity extends Activity{
    String[] list;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_explorer);

        updateList();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.listView1) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle("Options");
            String[] menuItems = getResources().getStringArray(R.array.contextMenu);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.contextMenu);
        String menuItemName = menuItems[menuItemIndex];

        File dir=new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/");
        File file = new File(dir, adapter.getItem(info.position));


        if(menuItemName.equals("Rename")) {
            createRenameMessage(dir, info).show();
            updateList();
        }else if(menuItemName.equals("Export")){
            Uri uri = Uri.fromFile(file);
            final Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(emailIntent);
        }else if(menuItemName.equals("Delete")){
            file.delete();
            updateList();
        }
        return true;
    }

    private void rename(File dir, String fromName, String toName){
        File from = new File(dir, fromName);
        File to = new File(dir, toName);
        from.renameTo(to);
    }

    private AlertDialog createRenameMessage(final File dir, final AdapterView.AdapterContextMenuInfo info){
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompt_rename, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set as new filename
                                String newName = userInput.getText().toString().replace(".gpx","");
                                rename(dir, adapter.getItem(info.position), newName + ".gpx");
                                // update GUI
                                updateList();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        return alertDialogBuilder.create();
    }

    private void updateList(){
        ListView listview = (ListView) findViewById(R.id.listView1);
        File mfile=new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/");
        list=mfile.list();
        adapter = new ArrayAdapter<>(this,R.layout.track_list_item,list);
        listview.setAdapter(adapter);
        registerForContextMenu(listview);
    }

}
