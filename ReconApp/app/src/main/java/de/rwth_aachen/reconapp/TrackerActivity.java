package de.rwth_aachen.reconapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.ResourceProxyImpl;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TrackerActivity extends Activity implements LocationListener{

    // constants
    private final static int MIN_ZOOM = 14;
    private final static int MAX_ZOOM = 19;
    private static final GeoPoint dummy = new GeoPoint(50.77909757775207,6.060287654399872);
    private final static int ACTIVITY_NOTSTARTED = 0;
    private final static int ACTIVITY_ACTIVE = 1;
    private final static int ACTIVITY_PAUSED = 2;

    // variables
    private LocationManager locationManager;
    private Track track;
    private TrackRecord trackRecord;
    private long lastUpdatedTrackRecord;
    private long lastUpdatedMapPath;

    private boolean activeGhost = false;
    private Overlay mMyLocationOverlay;
    private DefaultResourceProxyImpl mResourceProxy;
    private MapView myOpenMapView;
    private int currentZoom;
    private Drawable currentPositionMarker;
    private Drawable trackPointMarker;
    private Drawable ghostPositionMarker;
    private Timestamp startTime;
    private int curGhostPoint;
    private int curTrackPoint;
    private IMapController myMapController;
    private int activityStatus;
    private Button button, pauseButton;
    private long totalPause;
    private long startPause;
    private PathOverlay myPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        String trackname = "";
        if(b!=null){
           trackname =(String) b.get("track");
        }
        // load ghost track if available
        if(!trackname.equals("")) {
            track = new Track(trackname);
            // enable only iff at least two track points are available
            activeGhost = true;
        }

        setContentView(R.layout.activity_tracker);
        MyLocationListener locationListener = new MyLocationListener(this);
        mResourceProxy = new ResourceProxyImpl(getApplicationContext());

        // create new track record
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        trackRecord = new TrackRecord("track_"+currentDateandTime);


        //Bind GPS sensor
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try{
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,1, locationListener);
        }catch (Exception e){
            e.printStackTrace();
        }

        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        GeoPoint point = dummy;
        if(lastKnownLocation!=null) {
            point = new GeoPoint(lastKnownLocation);
        }
        initMap(point);

        ImageButton imgbutton = (ImageButton) findViewById(R.id.cambtn);
        if(imgbutton != null){
            imgbutton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v){
                    Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                    startActivity(intent);
                }
            });
        }

        pauseButton = (Button) findViewById(R.id.temp);
        if(pauseButton!=null) {
            pauseButton.setVisibility(View.INVISIBLE);
        }

        myPath = new PathOverlay(Color.CYAN, this);
        myPath.getPaint().setStrokeWidth(5.0f);
        updateGUI();
    }

    /**
     * Updates the buttons and the textout depending on the global acitivty status
     */
    private void updateGUI(){
        Location tmpLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(tmpLoc != null) {
            print(tmpLoc);
        }else{
            TextView tv_distance = (TextView) this.findViewById(R.id.distance);
            tv_distance.setText("GPS signal not available!");
        }

        button = (Button) findViewById(R.id.button);
        switch (activityStatus){
            case ACTIVITY_NOTSTARTED: //Activity not started
                if(button != null) {
                    button.setText("Start");
                    final Activity context = this;
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            // Perform action on click
                            startTime = new Timestamp(new Date().getTime());
                            curTrackPoint = 1;
                            curGhostPoint = 1;
                            totalPause = 0;
                            lastUpdatedTrackRecord = 0;
                            lastUpdatedMapPath = 0;
                            myPath = new PathOverlay(Color.CYAN, context);
                            myPath.getPaint().setStrokeWidth(5.0f);
                            activityStatus = ACTIVITY_ACTIVE;
                            updateGUI();
                        }
                    });
                }
                break;
            case ACTIVITY_ACTIVE: //Activity active
                if(button != null) {
                    button.setText("Pause");
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            // Perform action on click
                            activityStatus = ACTIVITY_PAUSED;
                            StartPause();
                            updateGUI();

                            if(pauseButton != null) {
                                pauseButton.setVisibility(View.VISIBLE);
                                pauseButton.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        pauseButton.setVisibility(View.INVISIBLE);
                                        activityStatus = ACTIVITY_ACTIVE;
                                        StopPause();
                                        updateGUI();
                                    }
                                });
                            }
                        }
                    });
                }
                break;
            case ACTIVITY_PAUSED: //Activity paused
                if(button != null) {
                    button.setText("Stop");
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            stopRecording();
                            updateGUI();
                        }
                    });
                }
                break;
        }
    }

    /**
     * stopps the recording
     */
    private void stopRecording(){
        activityStatus = ACTIVITY_NOTSTARTED;
        trackRecord.writeTrackToGPXFile();
        Toast.makeText(getApplicationContext(), "track saved", Toast.LENGTH_SHORT).show();

        // create new track record (to start new recording)
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        trackRecord = new TrackRecord(getApplicationContext() + "" + currentDateandTime);

        if(pauseButton != null) {
            pauseButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myOpenMapView.getTileProvider().detach();
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null)
        print(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * print the current position, updates the textoutput and the map
     * @param l
     */
    public void print(Location l){
        addLocationToTrackRecord(l);
        updateTrackPoint(l);
        updateGhost();
        updateMap(l);

        TextView tv_distance = (TextView) this.findViewById(R.id.distance);
        if(activeGhost) {
            float dis = l.distanceTo(track.getLocation(curTrackPoint));
            if (activityStatus == ACTIVITY_PAUSED) {
                tv_distance.setText("distance: " + Math.round(dis) + "m | paused!");
            } else if (activityStatus == ACTIVITY_NOTSTARTED) {
                tv_distance.setText("distance: " + Math.round(dis) + "m | not started!");
            } else {
                tv_distance.setText("distance: " + Math.round(dis) + "m | time left: " + (getTimeOffsetFromStart() - track.getTimeOffsetFromStart(curTrackPoint)) / 1000);
            }
        }else{
            if (activityStatus == ACTIVITY_PAUSED) {
                tv_distance.setText("paused!");
            } else if (activityStatus == ACTIVITY_NOTSTARTED) {
                tv_distance.setText("not started!");
            } else {
                tv_distance.setText("recording");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) return true;
        return super.onOptionsItemSelected(item);
    }

    private void StartPause(){
        System.out.println("Pause started");
        Timestamp curTime = new Timestamp(new Date().getTime());
        startPause = curTime.getTime();
    }

    private void StopPause(){
        System.out.println("Pause stopped");
        Timestamp curTime = new Timestamp(new Date().getTime());
        long endPause = curTime.getTime();
        totalPause += endPause - startPause;
    }

    public long getTotalPause(){
        if(activityStatus == ACTIVITY_PAUSED){
            // if activity currently paused, add current pause to total pause
            Timestamp curTime = new Timestamp(new Date().getTime());
            return totalPause + (curTime.getTime() - startPause);
        }
        return totalPause;
    }

    private long getTimeOffsetFromStart(){
        if(startTime == null){
            return 0;
        }
        Timestamp curTime = new Timestamp(new Date().getTime());
        return curTime.getTime() - getTotalPause() - startTime.getTime();
    }

    private void increaseZoom(){
        if(currentZoom == MAX_ZOOM){
            currentZoom = MIN_ZOOM;
        }else{
            currentZoom++;
        }
        myMapController.setZoom(currentZoom);
    }

    private void decreaseZoom(){
        if(currentZoom == MIN_ZOOM){
            currentZoom = MAX_ZOOM;
        }else{
            currentZoom--;
        }
        myMapController.setZoom(currentZoom);
    }

    private ArrayList<OverlayItem> addOverlayItem(GeoPoint point, Drawable marker, ArrayList<OverlayItem> items){
        OverlayItem olItem = new OverlayItem("Here", "SampleDescription", point);
        olItem.setMarker(marker);
        items.add(olItem);
        return items;
    }

    private void initMap(GeoPoint curPoint){
        currentPositionMarker = this.getResources().getDrawable(R.drawable.marker_blue);
        trackPointMarker = this.getResources().getDrawable(R.drawable.marker_default);
        ghostPositionMarker = this.getResources().getDrawable(R.drawable.marker_pink);

        myOpenMapView = (MapView) findViewById(R.id.openmapview);
        myOpenMapView.setBuiltInZoomControls(true);
        myMapController = myOpenMapView.getController();
        myMapController.setInvertedTiles(true);
        currentZoom = 18;
        myMapController.setZoom(currentZoom);

        ArrayList<OverlayItem> items = new ArrayList<>();
        items = addOverlayItem(curPoint, currentPositionMarker, items); //Current Location

        if(activeGhost) {
            // preload / cache map
            for (int i = 1; i < track.getLocationListSize(); i++) {
                myMapController.setCenter(new GeoPoint(track.getLocation(i)));
            }

            GeoPoint trackPoint = dummy;
            if (track.getLocationListSize()>0) {
                trackPoint = new GeoPoint(track.getLocation(0));
            }
            items = addOverlayItem(trackPoint, trackPointMarker, items); //Track Point
        }

        mMyLocationOverlay = new ItemizedIconOverlay<>(items, null, mResourceProxy);
        myOpenMapView.getOverlays().add(this.mMyLocationOverlay);
        myOpenMapView.invalidate();
        // center map on current position
        myMapController.setCenter(curPoint);
    }

    private void updateGhost(){
        if(!activeGhost){
            return;
        }
        // move ghost forward
        while(getTimeOffsetFromStart() > track.getTimeOffsetFromStart(curGhostPoint+1)){
            curGhostPoint++;
        }
    }

    private void updateMap(Location curLoc){
        ArrayList<OverlayItem> items = new ArrayList<>();

        GeoPoint point = new GeoPoint(curLoc);
        myMapController.setCenter(point);
        addOverlayItem(point, currentPositionMarker, items);

        if(activeGhost) {
            GeoPoint point2 = new GeoPoint(track.getLocation(curTrackPoint));
            addOverlayItem(point2, trackPointMarker, items);
            GeoPoint curGhostLocation = new GeoPoint(track.getLocation(curGhostPoint));
            addOverlayItem(curGhostLocation, ghostPositionMarker, items);
        }

        this.mMyLocationOverlay = new ItemizedIconOverlay<>(items, null, mResourceProxy);
        myOpenMapView.getOverlays().clear();
        myOpenMapView.getOverlays().add(this.mMyLocationOverlay);
        myOpenMapView.getOverlays().add(myPath);
        myOpenMapView.invalidate();
    }

    private void addLocationToTrackRecord(Location l){
        if(activityStatus != ACTIVITY_ACTIVE){
            return;
        }
        long timeOffset = getTimeOffsetFromStart();
        if( (timeOffset-lastUpdatedTrackRecord) > 5000){
            MyLocation newLoc = new MyLocation(l.getLatitude(), l.getLongitude(), new Timestamp(timeOffset));
            trackRecord.addLocation(newLoc);
            lastUpdatedTrackRecord = timeOffset;
        }
        if( (timeOffset-lastUpdatedMapPath) > 500){
            myPath.addPoint(new GeoPoint(l));
            lastUpdatedMapPath = timeOffset;
        }
    }

    @Override
    /**
     * Defines how actions on the control elements are handeled
     */
    public boolean dispatchKeyEvent(KeyEvent event) {
        int eventKeyCode = event.getKeyCode();

        int mOverrideKeyCodes[] = new int[] {
                KeyEvent.KEYCODE_DPAD_CENTER,
                KeyEvent.KEYCODE_DPAD_UP,
                KeyEvent.KEYCODE_DPAD_LEFT,
                KeyEvent.KEYCODE_DPAD_DOWN,
                KeyEvent.KEYCODE_DPAD_RIGHT
        };
        //19 = Down to Up
        //20 = Up to down
        //21 = Nose to ear
        //22 = Ear to nose
        //23 = "Select"
        if(eventKeyCode != 23) {
            for (int i = 0; i < mOverrideKeyCodes.length; i++) {
                if (eventKeyCode == mOverrideKeyCodes[i]) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        if(eventKeyCode == 19){
                            decreaseZoom();
                        }
                        if(eventKeyCode == 20){
                            increaseZoom();
                        }
                        if(eventKeyCode == 21) {
                            // start second screen
                            Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                            startActivity(intent);
                        }
                        if(eventKeyCode == 22) {
                            if(activityStatus == ACTIVITY_ACTIVE){
                                activityStatus = ACTIVITY_PAUSED;
                                StartPause();
                                updateGUI();
                            }else if(activityStatus ==ACTIVITY_PAUSED){
                                activityStatus = ACTIVITY_ACTIVE;
                                StopPause();
                                updateGUI();
                            }
                        }
                    }
                    return true;
                }
            }
        }

        return super.dispatchKeyEvent(event);
    }

    private void updateTrackPoint(Location curLoc){
        if(!activeGhost || activityStatus!=ACTIVITY_ACTIVE){
            return;
        }
        // choose nearest point in next 30 seconds as track point
        float tmpDis;
        MyLocation tmpLoc;
        float minDis = Float.MAX_VALUE;
        int minIndex = curTrackPoint;
        // move track point forward
        for(int i = curTrackPoint+1; i < track.getLocationListSize()  && (track.getTimeOffsetFromStart(i)-track.getTimeOffsetFromStart(curTrackPoint)) < 30000; i++ ){
            tmpLoc = track.getLocation(i);
            tmpDis = curLoc.distanceTo(tmpLoc);
            if(tmpDis < minDis){
                minDis = tmpDis;
                minIndex = i;
            }
        }

        float tmpCurTrackToMin = track.getLocation(curTrackPoint).distanceTo(track.getLocation(minIndex));
        float tmpCurLocToCurTrack = curLoc.distanceTo(track.getLocation(curTrackPoint));
        float tmpCurLocToMinTrack = curLoc.distanceTo(track.getLocation(minIndex));
        if(tmpCurLocToMinTrack<tmpCurLocToCurTrack || (tmpCurLocToMinTrack<tmpCurTrackToMin) ) {
            long relativeTime = getTimeOffsetFromStart() - track.getTimeOffsetFromStart(curTrackPoint);
            Toast.makeText(getApplicationContext(),"Relative Time: "+relativeTime/1000,Toast.LENGTH_SHORT).show();
            curTrackPoint = minIndex;
        }
        // check for final trackpoint (in range of 5 meters)
        Location lastLoc = track.getLocation(track.getLocationListSize()-1);
        if(activityStatus == ACTIVITY_ACTIVE) {
            if (curLoc.distanceTo(lastLoc) < 5 && (track.getTimeOffsetFromStart(track.getLocationListSize() - 1) - track.getTimeOffsetFromStart(curTrackPoint)) < 30000) {
                stopRecording();
                Toast.makeText(getApplicationContext(), "Finished track", Toast.LENGTH_SHORT).show();
                updateGUI();
            }
        }

    }
}
