package de.rwth_aachen.reconapp;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.widget.ArrayAdapter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * Provides functions to send and receive GPX files
 * Created by admin on 12.08.2015.
 */
public class BluetoothUtility {

    // variables
    private static HashMap<String,String> tempFiles;

    public static void init(){
        tempFiles = new HashMap<>();
    }

    /**
     * Creates message as byte array from file
     * @param file file
     * @param filename filename
     * @param chatService receiver for write call
     */
    public static void sendMessage(File file, String filename, BluetoothChatService chatService){
        // Format: ***filename*currentPacket*maxPacket**payload***
        try {
            int fileLength = (int)file.length();
            byte[] bytesFile = new byte[fileLength];
            // use BufferedInputStream to read file
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytesFile, 0, fileLength);
            buf.close();
            String content = new String(bytesFile, Charset.defaultCharset());
            // Create messages (400 bytes per message)
            String parts[] = splitStringEvery(content,400);
            int counter = 1;
            for(String part: parts){
                String partMessage="";
                partMessage+="***";
                partMessage+=filename;
                partMessage+="*";
                partMessage+=counter;
                partMessage+="*";
                partMessage+=parts.length;
                partMessage+="**";
                partMessage+=part;
                partMessage+="***";
                chatService.write(partMessage.getBytes());
                Thread.sleep(100);
                counter++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates control message to receive tracks
     * @param chatService receiver for write call
     */
    public static void sendMessage2(BluetoothChatService chatService){
        // Format: *** *-1*-1** ***
        try {
            // Create control message
            String partMessage="*** *-1*-1** ***";
            chatService.write(partMessage.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receives file part as message and combine to whole file written to SD
     * @param message message containing file part
     */
    public static synchronized String receiveMessage(String message, ArrayAdapter<String> outputAdapter, Context context){
        // Format: ***filename*currentPacket*maxPacket**payload***
        String filename = "";
        try {
            // check format
            if(!message.startsWith("***") || !message.endsWith("***")){
                // wrong message format / receive incomplete
                return "";
            }
            String innerMessage = message.split("\\*\\*\\*")[1];
            String headerPayload[] = innerMessage.split("\\*\\*");
            String header[] = headerPayload[0].split("\\*");
            filename = header[0];
            int curPacket = Integer.parseInt(header[1]);
            int maxPacket = Integer.parseInt(header[2]);
            String content = headerPayload[1];

            // Add content to temporary files
            if(tempFiles.containsKey(filename)){
                String tmp = tempFiles.get(filename);
                tmp += content;
                tempFiles.put(filename,tmp);
            }else{
                tempFiles.put(filename,content);
            }

            // Write track file if receive complete
            if(curPacket==maxPacket) {
                File file = null;
                if(filename.contains("gpx")) {
                    file = new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/" + filename);
                    outputAdapter.add("Received track: "+ filename);
                }else if(filename.contains("jpg")){
                    file = new File(Environment.getExternalStorageDirectory() + "/Documents/recon/pictures/" + filename);
                    outputAdapter.add("Received picture: "+ filename);
                }
                if(file != null) {
                    BufferedOutputStream buf = new BufferedOutputStream(new FileOutputStream(file));
                    buf.write(tempFiles.get(filename).getBytes(), 0, tempFiles.get(filename).getBytes().length);
                    buf.close();
                    MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, null);
                }
                tempFiles.remove(filename);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filename;
    }


    /**
     * Splits string in substrings based on interval
     * @param s given String
     * @param interval split every specified number
     * @return substrings as array
     */
    private static String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }

}
