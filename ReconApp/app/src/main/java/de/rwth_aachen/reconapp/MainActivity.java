
package de.rwth_aachen.reconapp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.Locale;

public class MainActivity extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            switch (position) {
                case 0:
                    Fragment fragment = new DummySectionFragment1();
                    Bundle args = new Bundle();
                    args.putInt("section0", position + 1);
                    fragment.setArguments(args);
                    return fragment;

                case 1:
                    Fragment fragment2 = new DummySectionFragment2();
                    args = new Bundle();
                    args.putInt("section2", position + 2);
                    fragment2.setArguments(args);
                    return fragment2;

                case 2:

                    Fragment fragment3 = new DummySectionFragment3();
                    args = new Bundle();
                    args.putInt("section3", position + 3);
                    fragment3.setArguments(args);
                    return fragment3;


                default:
                    return null;
            }


        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    public static class DummySectionFragment1 extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public DummySectionFragment1() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main0, container, false);

            final ImageButton button = (ImageButton) rootView.findViewById(R.id.imageButton);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // start second screen
                    Intent intent = new Intent(getActivity().getApplicationContext(), SelectTrackActivity.class);
                    startActivity(intent);
                }
            });
            return rootView;
        }
    }

    public static class DummySectionFragment2 extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public DummySectionFragment2() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main1, container, false);

            final ImageButton button = (ImageButton) rootView.findViewById(R.id.imageButton);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Then you start a new Activity via Intent
                    Intent intent = new Intent();
                    intent.setClass(getActivity().getApplicationContext(), TrackerActivity.class);
                    intent.putExtra("track", "");
                    startActivity(intent);
                }
            });

            return rootView;
        }
    }

    public static class DummySectionFragment3 extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public DummySectionFragment3() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main2, container, false);

            final ImageButton button = (ImageButton) rootView.findViewById(R.id.imageButton);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // start second screen
                    Intent intent = new Intent(getActivity().getApplicationContext(), SyncTracks.class);
                    startActivity(intent);
                }
            });
            return rootView;
        }
    }

}
