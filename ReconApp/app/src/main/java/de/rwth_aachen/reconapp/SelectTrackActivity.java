package de.rwth_aachen.reconapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;

/**
 * Created by Thomas on 12.07.2015.
 */
public class SelectTrackActivity extends Activity implements AdapterView.OnItemClickListener {
    String[] list;
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //creates a list view containing all files saved in /Documents/recon/tracks
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_track);
        //* *EDIT* *
        ListView listview = (ListView) findViewById(R.id.listView1);
        listview.setOnItemClickListener(this);
        File mfile=new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/");
        list=mfile.list();
        adapter = new ArrayAdapter<>(this,R.layout.track_list_item,list);
        listview.setAdapter(adapter);
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        // Then you start a new Activity via Intent and passing the trackname to the other Activity
        Intent intent = new Intent();
        intent.setClass(this, TrackerActivity.class);
        intent.putExtra("track", adapter.getItem(position));
        intent.putExtra("id", id);
        startActivity(intent);
    }


}
