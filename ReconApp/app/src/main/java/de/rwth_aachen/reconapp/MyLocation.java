package de.rwth_aachen.reconapp;

import android.location.Location;

import java.sql.Timestamp;

/**
 * Created by Thomas on 23.06.2015.
 * Represents a single Location inlcuding a timestamp
 */
public class MyLocation extends Location {

    private Timestamp time;

    public MyLocation(double latitude, double longitude, Timestamp time) {
        super("GPS");
        super.setLatitude(latitude);
        super.setLongitude(longitude);
        this.time = time;
    }

    @Override
    public long getTime() {
        return time.getTime();
    }

    @Override
    public String toString() {
        return "GPS: "+getLatitude()+" / "+getLongitude()+" / "+time;
    }

    public Timestamp getTimestamp(){
        return time;
    }
}
