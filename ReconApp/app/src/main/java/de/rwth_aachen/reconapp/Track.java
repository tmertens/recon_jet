package de.rwth_aachen.reconapp;

import android.os.AsyncTask;
import android.os.Environment;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Manages existing tracks
 * Created by Thomas on 23.06.2015.
 */
public class Track {

    // variables
    private List<MyLocation> locationList;
    private String filename;

    /**
     * Default constructor
     * @param filename track name
     */
    public Track(String filename) {
        locationList = new ArrayList<>();
        this.filename = filename;



        // load GPX file in AsyncTask
        new GPX().execute();
    }

    /**
     *
     * @param i location index
     * @return location
     */
    public MyLocation getLocation(int i){
        return locationList.get(i);
    }

    /**
     * Returns number of locations
     * @return number of locations
     */
    public int getLocationListSize(){
        return locationList.size();
    }

    public List getLocationList() {return locationList;}

    /**
     * AsyncTask to load GPX file in background
     */
    private class GPX extends AsyncTask<URL, String, String> {

        @Override
        protected String doInBackground(URL... params) {
            try {
                XmlPullParserFactory parserFactory;
                parserFactory = XmlPullParserFactory.newInstance();
                XmlPullParser parser = parserFactory.newPullParser();
                File dir = new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/");
                //filename = filename.replace(".gpx","");
                //File file = new File(dir, filename+".gpx");
                File file = new File(dir, filename);
                InputStream fin = new FileInputStream(file);
                parser.setInput(fin,null);
                // init variables
                int parserEvent = parser.getEventType();
                float lat = -1;
                float lon = -1;
                String tmpString;
                Timestamp time;
                // start parsing XML
                while (parserEvent != XmlPullParser.END_DOCUMENT) {
                    switch (parserEvent) {
                        case XmlPullParser.START_TAG:
                            String tag = parser.getName();
                            if (tag.compareTo("trkpt") == 0) {
                                // first get lat and lon
                                lat = Float.parseFloat(parser.getAttributeValue(null, "lat"));
                                lon = Float.parseFloat(parser.getAttributeValue(null, "lon"));
                            } else if (tag.compareTo("time") == 0){
                                // then timestamp
                                tmpString = parser.nextText();
                                // replace T/Z to parse ISO 8601 timestamps
                                tmpString = tmpString.replace("T"," ");
                                tmpString = tmpString.replace("Z", " ");
                                time = Timestamp.valueOf(tmpString);
                                MyLocation ml = new MyLocation(lat,lon,time);
                                locationList.add(ml);
                                System.out.print(ml);
                            }
                            break;
                    }
                    parserEvent = parser.next();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
            return "";
        }
    }

    /**
     * Calculates time in ms from start to a specific point
     * @param i position index
     * @return time offset from start in ms
     */
    public long getTimeOffsetFromStart(int i){
        if(i >= locationList.size()){
            return Long.MAX_VALUE;
        }
        long t1 = getLocation(0).getTime();
        long t2 = getLocation(i).getTime();
        return t2-t1;
    }
}
