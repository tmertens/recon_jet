package de.rwth_aachen.reconapp;

import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * Manages new track record
 * Created by admin on 02.07.2015.
 */
public class TrackRecord {

    // variables
    private List<MyLocation> locationList;
    private String filename;

    /**
     * defaut constructor
     * @param filename new track name
     */
    public TrackRecord(String filename) {
        locationList = new ArrayList<>();
        this.filename = filename;
    }

    /**
     * Adds new location to list
     * @param newLoc new location
     */
    public void addLocation(MyLocation newLoc){
        locationList.add(newLoc);
    }

    /**
     * Exports track to GPX file
     */
    public void writeTrackToGPXFile(){
        try {
            //file = new File(context.getFilesDir(), filename+".gpx");
            File dir = new File(Environment.getExternalStorageDirectory() + "/Documents/recon/tracks/");
            dir.mkdir();
            File file = new File(dir, filename + ".gpx");
            OutputStream fout = new FileOutputStream(file);
            fout.write(createGPXString().getBytes());
            fout.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates GPX format as String
     * @return Returns the whole gpx file
     */
    private String createGPXString(){
        String result="";
        result += "<gpx>\n";
        result += "<trk>\n";
        result += "<name>"+filename+"</name>\n";
        result += "<desc> </desc>\n";
        result += "<trkseq>\n";
        // add all locations
        for(MyLocation l: locationList){
            result += "<trkpt lat=\""+l.getLatitude()+"\" lon=\""+l.getLongitude()+"\">\n"+
                    "   <time>"+getISO8601StringForDate(l.getTimestamp())+"</time>\n"+
                    "</trkpt>\n";
        }
        result += "</trkseq>\n";
        result += "</trk>\n";
        result += "</gpx>\n";
        return result;
    }

    /**
     * Converts timestamp in ISO8601 representation
     * @param ts timestamp
     * @return String in ISO8601 Format
     */
    private static String getISO8601StringForDate(Timestamp ts) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(ts);
    }


    @Override
    public String toString() {
        return ""+filename+": "+locationList;
    }
}
