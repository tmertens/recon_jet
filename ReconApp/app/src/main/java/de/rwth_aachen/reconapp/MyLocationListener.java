package de.rwth_aachen.reconapp;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by admin on 30.06.2015.
 */
public class MyLocationListener implements LocationListener{

    private final TrackerActivity activity;

    public MyLocationListener(TrackerActivity activity) {
        this.activity = activity;
    }

    /**
     * the function print is called at every time the location changes
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        activity.print(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}


